-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 17-04-2018 a las 03:29:51
-- Versión del servidor: 10.1.21-MariaDB
-- Versión de PHP: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `pokemon_super`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Effect`
--

CREATE TABLE `Effect` (
  `id` int(11) NOT NULL,
  `description` varchar(45) NOT NULL,
  `logo` varchar(45) NOT NULL,
  `name` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `Effect`
--

INSERT INTO `Effect` (`id`, `description`, `logo`, `name`) VALUES
(1, 'Heal all pokemons up to 2', 'nein', 'Heal Master'),
(2, 'Damage all foes by 3', 'nein', 'Crusader'),
(3, 'Damage all pokemons by 1', 'nein', 'Suicide Squad');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Envelope`
--

CREATE TABLE `Envelope` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `price` int(11) NOT NULL,
  `pokemonChance` int(11) NOT NULL,
  `stoneChance` int(11) NOT NULL,
  `itemChance` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Evolution`
--

CREATE TABLE `Evolution` (
  `id` int(11) NOT NULL,
  `from` int(11) NOT NULL,
  `to` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `stone` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Item`
--

CREATE TABLE `Item` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `description` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `NullAgainst`
--

CREATE TABLE `NullAgainst` (
  `me` int(11) NOT NULL,
  `other` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Pokemon`
--

CREATE TABLE `Pokemon` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `description` varchar(256) NOT NULL,
  `hp` int(11) NOT NULL,
  `attack` int(11) NOT NULL,
  `effect_id` int(11) DEFAULT NULL,
  `dropChance` int(11) NOT NULL,
  `image` varchar(128) NOT NULL,
  `sound` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `Pokemon`
--

INSERT INTO `Pokemon` (`id`, `name`, `description`, `hp`, `attack`, `effect_id`, `dropChance`, `image`, `sound`) VALUES
(1, 'Pikachu', 'rata', 4, 2, NULL, 10, 'null', 'null');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `PokemonType`
--

CREATE TABLE `PokemonType` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Pokemon_has_PokemonType`
--

CREATE TABLE `Pokemon_has_PokemonType` (
  `Pokemon_id` int(11) NOT NULL,
  `PokemonType_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Stone`
--

CREATE TABLE `Stone` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `description` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `StrongAgainst`
--

CREATE TABLE `StrongAgainst` (
  `me` int(11) NOT NULL,
  `other` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Trainer`
--

CREATE TABLE `Trainer` (
  `id` int(11) NOT NULL,
  `username` varchar(45) NOT NULL,
  `email` varchar(45) NOT NULL,
  `password` varchar(128) NOT NULL,
  `name` varchar(45) NOT NULL,
  `level` int(11) NOT NULL DEFAULT '1',
  `exp` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `Trainer`
--

INSERT INTO `Trainer` (`id`, `username`, `email`, `password`, `name`, `level`, `exp`) VALUES
(1, 'ashk', 'some@some.com', '1234', 'Ash', 1, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Trainer_has_Pokemon`
--

CREATE TABLE `Trainer_has_Pokemon` (
  `Trainer_id` int(11) NOT NULL,
  `Pokemon_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `Trainer_has_Pokemon`
--

INSERT INTO `Trainer_has_Pokemon` (`Trainer_id`, `Pokemon_id`) VALUES
(1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `WeakAgainst`
--

CREATE TABLE `WeakAgainst` (
  `me` int(11) NOT NULL,
  `other` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `Effect`
--
ALTER TABLE `Effect`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `Envelope`
--
ALTER TABLE `Envelope`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name_UNIQUE` (`name`);

--
-- Indices de la tabla `Evolution`
--
ALTER TABLE `Evolution`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_Evolution_Pokemon1_idx` (`from`),
  ADD KEY `fk_Evolution_Pokemon2_idx` (`to`),
  ADD KEY `fk_Evolution_Stone1_idx` (`stone`);

--
-- Indices de la tabla `Item`
--
ALTER TABLE `Item`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `NullAgainst`
--
ALTER TABLE `NullAgainst`
  ADD PRIMARY KEY (`me`,`other`),
  ADD KEY `fk_NullAgainst_PokemonType2_idx` (`other`);

--
-- Indices de la tabla `Pokemon`
--
ALTER TABLE `Pokemon`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name_UNIQUE` (`name`),
  ADD KEY `fk_Pokemon_Effect1_idx` (`effect_id`);

--
-- Indices de la tabla `PokemonType`
--
ALTER TABLE `PokemonType`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `Pokemon_has_PokemonType`
--
ALTER TABLE `Pokemon_has_PokemonType`
  ADD PRIMARY KEY (`Pokemon_id`,`PokemonType_id`),
  ADD KEY `fk_Pokemon_has_PokemonType_PokemonType1_idx` (`PokemonType_id`),
  ADD KEY `fk_Pokemon_has_PokemonType_Pokemon1_idx` (`Pokemon_id`);

--
-- Indices de la tabla `Stone`
--
ALTER TABLE `Stone`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name_UNIQUE` (`name`);

--
-- Indices de la tabla `StrongAgainst`
--
ALTER TABLE `StrongAgainst`
  ADD PRIMARY KEY (`me`,`other`),
  ADD KEY `fk_StrongAgainst_PokemonType2_idx` (`other`);

--
-- Indices de la tabla `Trainer`
--
ALTER TABLE `Trainer`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username_UNIQUE` (`username`),
  ADD UNIQUE KEY `email_UNIQUE` (`email`);

--
-- Indices de la tabla `Trainer_has_Pokemon`
--
ALTER TABLE `Trainer_has_Pokemon`
  ADD PRIMARY KEY (`Trainer_id`,`Pokemon_id`),
  ADD KEY `fk_Trainer_has_Pokemon_Pokemon1_idx` (`Pokemon_id`),
  ADD KEY `fk_Trainer_has_Pokemon_Trainer1_idx` (`Trainer_id`);

--
-- Indices de la tabla `WeakAgainst`
--
ALTER TABLE `WeakAgainst`
  ADD PRIMARY KEY (`me`,`other`),
  ADD KEY `fk_WeakAgainst_PokemonType2_idx` (`other`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `Effect`
--
ALTER TABLE `Effect`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `Envelope`
--
ALTER TABLE `Envelope`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `Evolution`
--
ALTER TABLE `Evolution`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `Item`
--
ALTER TABLE `Item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `Pokemon`
--
ALTER TABLE `Pokemon`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT de la tabla `PokemonType`
--
ALTER TABLE `PokemonType`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `Stone`
--
ALTER TABLE `Stone`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `Trainer`
--
ALTER TABLE `Trainer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `Evolution`
--
ALTER TABLE `Evolution`
  ADD CONSTRAINT `fk_Evolution_Pokemon1` FOREIGN KEY (`from`) REFERENCES `Pokemon` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Evolution_Pokemon2` FOREIGN KEY (`to`) REFERENCES `Pokemon` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Evolution_Stone1` FOREIGN KEY (`stone`) REFERENCES `Stone` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `NullAgainst`
--
ALTER TABLE `NullAgainst`
  ADD CONSTRAINT `fk_NullAgainst_PokemonType1` FOREIGN KEY (`me`) REFERENCES `PokemonType` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_NullAgainst_PokemonType2` FOREIGN KEY (`other`) REFERENCES `PokemonType` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `Pokemon`
--
ALTER TABLE `Pokemon`
  ADD CONSTRAINT `fk_Pokemon_Effect1` FOREIGN KEY (`effect_id`) REFERENCES `Effect` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `Pokemon_has_PokemonType`
--
ALTER TABLE `Pokemon_has_PokemonType`
  ADD CONSTRAINT `fk_Pokemon_has_PokemonType_Pokemon1` FOREIGN KEY (`Pokemon_id`) REFERENCES `Pokemon` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Pokemon_has_PokemonType_PokemonType1` FOREIGN KEY (`PokemonType_id`) REFERENCES `PokemonType` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `StrongAgainst`
--
ALTER TABLE `StrongAgainst`
  ADD CONSTRAINT `fk_StrongAgainst_PokemonType1` FOREIGN KEY (`me`) REFERENCES `PokemonType` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_StrongAgainst_PokemonType2` FOREIGN KEY (`other`) REFERENCES `PokemonType` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `Trainer_has_Pokemon`
--
ALTER TABLE `Trainer_has_Pokemon`
  ADD CONSTRAINT `fk_Trainer_has_Pokemon_Pokemon1` FOREIGN KEY (`Pokemon_id`) REFERENCES `Pokemon` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Trainer_has_Pokemon_Trainer1` FOREIGN KEY (`Trainer_id`) REFERENCES `Trainer` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `WeakAgainst`
--
ALTER TABLE `WeakAgainst`
  ADD CONSTRAINT `fk_WeakAgainst_PokemonType1` FOREIGN KEY (`me`) REFERENCES `PokemonType` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_WeakAgainst_PokemonType2` FOREIGN KEY (`other`) REFERENCES `PokemonType` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
